fun main() {
    var sum = 0
    var arr = intArrayOf(1,2,3,4,5,6,7,8)

    for (num in arr) {
        sum += num
    }
    val avrg = sum / arr.size

    println(avrg)

}